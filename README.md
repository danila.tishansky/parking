# MyParking

### Разработка приложения по поддержке платного парковочного пространства города по дисциплине "Технологии программирования"

## Команда разработчиков

ФКН, 3 курс, группа 5.2, команда 2

* Хныкин Данила Евгеньевич
* Филиппов Иван Юрьевич
* Тишанский Данила Александрович

## Демонстрационный аккаунт

* MyParking.apk - демонстрационная версия приложения
* логин: demo@d.ru
* пароль: 1234


## Сервисы

* [Документация в Miro](https://miro.com/app/board/uXjVOJXDScM=/)
* [Рабочее пространство в Trello](https://trello.com/b/AD4kxm8Z/parking)

## Видео

* [Обзор и деплой backend-приложения](https://www.youtube.com/watch?v=WBAGNdtSyHk)
* [Обзор frontend-приложения](https://www.youtube.com/watch?v=8_vP4sQA_B0)
* [Демо презентация](https://www.youtube.com/watch?v=u0zqNK_Fe9k)


## Документация

* [Функциональная схема в Miro ](https://miro.com/app/board/uXjVOJXDScM=/)
* [Техническое задание ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/tekhnicheskoe_zadanie.pdf)
* [API документация ](https://tp-myparking.herokuapp.com/swagger-ui/index.html#/) - приложение развернуто в сервисе
  heroku с бесплатным тарифом, поэтому сваггер может не сразу загрузиться, нужно подождать секунд 40, пока приложение
  стартанется
* [История версий ](https://gitlab.com/danila.tishansky/parking/-/blob/main/version-history.md)
* [Курсовая работа ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/kursovaya-rabota.pdf)
* [Презентация ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/presentation.pdf)

### Дизайн-макет приложения

* [Дизайн-макет приложения ](https://www.figma.com/community/file/1102272719950085258?fuid=902886657229412540)

### Диаграммы

* [ER-диаграмма ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/parking-db-er-diagram.png)
* [Схема БД ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/parking-db.png)
* [Диаграмма классов ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/class-diagram.png)
* [Диаграмма объектов ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/objects-diagram.png)
* [IDEF0-диаграмма ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/IDEF0-diagram.png)
* [Диаграмма состояний ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/statechart-diagram.png)
* [Use-case диаграмма ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/use-case-diagram.png)
* [Диаграмма активностей ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/activity-diagram.png)
* [Диаграмма последовательности ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/sequence-diagram.png)
* [Диаграмма развертывания ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/diagrams/deployment.png)

### Метрики

* [Воронка "Добавление автомобиля" ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/metrics/add-car-funnel.png)
* [Воронка "Оплата парковочного места" ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/metrics/pay-parking-funnel.png)
* [Воронка "Пополнение баланса кошелька" ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/metrics/replenish-balance-funnel.png)
* [Воронка "Оплата парковочного места гостем" ](https://gitlab.com/danila.tishansky/parking/-/blob/main/docs/metrics/pay-parking-anon-funnel.png)

## Ведение проекта

### Структура git репозитория

Репозиторий состоит из трех основных веток:

* **main**  — в ней хранится актуальная документация к проекту
* **frontend**  — в ней ведется разработка мобильного приложения
* **backend**  — в ней ведется разработка серверной части приложения

### Процесс разработки

Подробный процесс разработки фичи от планирования до реализации описан в
разделе [жизненный цикл задач ](https://gitlab.com/danila.tishansky/parking/-/blob/main/development.md)

### Релизы

#### Текущая версия backend - **v1.3.0**

Документирование релизов ведется в
разделе [история версий ](https://gitlab.com/danila.tishansky/parking/-/blob/main/version-history.md)
